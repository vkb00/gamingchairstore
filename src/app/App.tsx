import './App.css'
import Navbar from '../components/navbar'
import { Route, Routes } from 'react-router-dom'
import Home from './home'

function App() {

  return (
    <>

      <Routes>
        <Route path={''} element={<Home />} />

      </Routes>
    </>
  )
}

export default App
