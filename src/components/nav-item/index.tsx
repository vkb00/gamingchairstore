import { Link } from "react-router-dom";
import "./style.css"
interface PropsNavItem {
    title: string,
    link: string
}
const NavItem = ({ title, link }: PropsNavItem) => {
    return (
        <div className="NavItem">
            <Link to={link}>{title}</Link>
        </div>
    )
};
export default NavItem;
