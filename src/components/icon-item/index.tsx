import icon from "../../../public/imgs/icon.png"

import "./style.css"
interface PropsIconItem {
    title: string,
    description: string
}
const IconItem = ({ title, description }: PropsIconItem) => {
    return (
        <div className="iconItem">
            <img className="iconImg" src={icon} alt="" />
            <h2>{title}</h2>
            <p>{description}</p>
        </div>
    )
};
export default IconItem;
