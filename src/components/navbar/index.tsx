import NavItem from "../nav-item";
import "./style.css"
const Navbar = () => {
    return (
        <div className="Navbar">
            <img src="" alt="" />
            <div className="NavItems">
                <NavItem title={"ГЛАВНАЯ"} link={""} />
                <NavItem title={"КАТАЛОГ"} link={""} />
                <NavItem title={"ГДЕ КУПИТЬ"} link={""} />
                <NavItem title={"КОНТАКТЫ"} link={""} />


            </div>
        </div>
    )
};
export default Navbar;
